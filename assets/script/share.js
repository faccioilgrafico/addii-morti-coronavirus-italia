	window.Share=function(){
		function e(){}return e.prototype.init=function(){
			var e,t,n,i,r,o,s,a,l,u,d,c,k,w,z,p=this;
			return u=$("title").text(),
			l=$("meta[property='og:description']").attr("content"),
			//r=window.location.href.replace("#",""),
			r=window.location.href.split(/[?#]/)[0],
			n=$(".j-twitter").data("hashtags"),
			i=n&&n.length?"&hashtags="+n:"",
			d=$(".j-twitter").data("share")||u,s=$(".j-mail").data("subject")||$(".j-mail").data("share")||u,o=$(".j-mail").data("body")||r,e=encodeURI("http://www.facebook.com/sharer.php?u="+r),
			t=encodeURI("https://plus.google.com/share?url="+r+"&hl=it"),
			k=encodeURI("https://www.linkedin.com/shareArticle?mini=true&url="+r),
			w=encodeURI("whatsapp://send?text="+r),
			z=encodeURI("tg://msg?text="+r),
			c=encodeURI("http://twitter.com/share?text="+d+"&url="+r+i),
			a=encodeURI("mailto:?Subject="+s+"&Body="+o),
			$(".j-mail").attr("href",a),
			$(".j-facebook").data("href",e).click(function(e){
				return e.preventDefault(),
				p.openSharePopup($(e.currentTarget).data("href"))
			}),
			$(".j-twitter").data("href",c).click(function(e){
				return e.preventDefault(),
				p.openSharePopup($(e.currentTarget).data("href"))
			}),
			$(".j-linkedin").data("href",k).click(function(e){
				return e.preventDefault(),
				p.openSharePopup($(e.currentTarget).data("href"))
			}),
			$(".j-wa").data("href",w).click(function(e){
				return e.preventDefault(),
				p.openSharePopup($(e.currentTarget).data("href"))
			}),
			$(".j-tg").data("href",z).click(function(e){
				return e.preventDefault(),
				p.openSharePopup($(e.currentTarget).data("href"))
			}),
			$(".j-gplus").data("href",t).click(function(e){
				return e.preventDefault(),
				p.openSharePopup($(e.currentTarget).data("href"))
			})
		},
			e.prototype.openSharePopup=function(e){
				return window.open(e,"Share","toolbar=0,status=0,width=640,height=480")},e
			}(),
			share=new Share;

	$(document).ready(function(){
		share.init()
	});