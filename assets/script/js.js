<!--#include virtual="/includes2013/SSI/config/global.config" -->

returnToList=0;
hashNavigation = {};

$(function(){ // document ready
      
    var descrizioni = [];
    var total_number = descrizioni.length-1;
    var position =  0;
   
    hashNavigation.start = position;

    posHtml = $(".contatore p");
    //$('.fitText').bigtext();
    var options = {
        valueNames: [
            'voce-titolo',
            { data: [
                    'id',
                    'nome',
                    'eta',
                    'sesso',
                    'comune',
                    'regione',
                    'testo'
                ] }
        ]
    };

    var vociLista = new List('listaVoci', options );
    var activeFilters = [];

    var dataLayer = window.dataLayer = window.dataLayer || [];
    dataLayer.push({
      'pagename': 'Addii'
    });

function showDescription(lista, index, totale, click){
    click = (typeof click !== 'undefined') ?  click : true;
    //console.log(lista, index, totale);
    $('.vociList li').removeClass('active');
    $('.vociList li[data-id="' + lista[index] + '"]').addClass('active');
    if ($(".vociList li.active").hasClass('regione')) {
        $('.full .memoria').css('display','none');
        //$('.full .contatore-main').css('display','none');
    }
    else {
        $('.full .memoria').css('display','block');     
        //$('.full .contatore-main').css('display','block');
    }
    if(click){
        //window.location.hash = lista[index];
    }

    posHtml.html( "<strong>" + ( parseInt(index) + 1 ) + "</strong> di " + totale);
    position = index;   

}

// Filtri menu

    $('#filter_sesso').change(function() {
        vociLista.filter();
        //$('.regione').attr('data-always-visible','true');
        var value = this.value;
        if(value != 'all') {
        $('.regione').attr('data-sesso',value);
        vociLista.reIndex();
        $('.filtro select').not(this).prop('selectedIndex',0);
        $('.filtro button').removeClass('selected').addClass('all');
        vociLista.filter(function (item) {         

            if (item.values().sesso == value) {
                return true;
            } 
            else {
                return false;
            }
        });
        }
        else {
            vociLista.filter(); // Remove all filters
        }
        return false;
     });

    $('#filter_regione').change(function() {
        //vociLista.filter();
        // $('.regione').removeAttr('data-always-visible');
        // vociLista.reIndex();
        var value = this.value;
        if(value != 'all') {
        $('.filtro select').not(this).prop('selectedIndex',0);
        $('.filtro button').removeClass('selected').addClass('all');
        vociLista.filter(function (item) {
            if (item.values().regione == value) {
                return true;
            } else {
                return false;
            }
        });
        }
        else {
            vociLista.filter(); // Remove all filters
        }
        return false;
     });

/* Fine filtri */   

    vociLista.on('updated', function(elem){
        //console.log(elem.visibleItems);
        total_number =  elem.matchingItems.length;
        posHtml.html("0 di " + total_number);
      
        offset = $('#long_desc').offset().top - $(window).height();

        if( 0 == elem.matchingItems.length ){
            posHtml.html("nessun elemento");
            $('.contatore').css('display','none');
            $('.messages').html("<p class='notFound'>Spiacente, la ricerca non ha prodotto nessun risultato.</p>");
            descrizioni = [];
            position = 0;
        } else {
            $('.contatore').css('display','block');
            $('.messages').html("");
            descrizioni = [];
            position = 0;
            $.each(elem.matchingItems, function(k, v){
                descrizioni.push( v._values.id );
            });
        }
            showDescription(descrizioni, position, total_number);
    });
  
    vociLista.update();
    hashNavigation.total = vociLista.matchingItems.length;
     
    if( !(window.location.hash == null || window.location.hash == '' || window.location.hash == undefined) ){
        showDescription(descrizioni, window.location.hash.replace('#',''), hashNavigation.total);
        $("html, body").animate({ scrollTop: $('#long_desc').offset().top }, 1000);
    } else{
        showDescription(descrizioni, 0, total_number, false );
    }
    $('.contatore div').on('click', function(){
            if ( $(this).hasClass('prev') ){
                position--;
                if(position<0) { position = descrizioni.length-1; }
            } else if ( $(this).hasClass('next') ){
                position++;
                if(position>descrizioni.length-1) { position = 0; }
            }
            showDescription(descrizioni, position, total_number);
        });

if ($('body').hasClass('mobile')) {
        $('.descrizione').swipe( {
            swipeLeft:function(event, direction, distance, duration, fingerCount) {
                console.log(event, direction, distance, duration, fingerCount);
                position++;
                if(position>descrizioni.length-1) { position = 0; }
                showDescription(descrizioni, position, total_number);
            },
            swipeRight:function(event, direction, distance, duration, fingerCount) {
                position--;
                if(position<0) { position = descrizioni.length-1; }
                showDescription(descrizioni, position, total_number);
            },
            //Default 75px
            threshold:100,
            excludedElements:$.fn.swipe.defaults.excludedElements+", .social_voce"
        });  
}

if ($('body').hasClass('desktop')) {
    document.onkeydown = checkKey;
    function checkKey(e) {
        e = e || window.event;
        if (e.keyCode == '27') {
           // ESC
           $('span.close').trigger('click');
        }
        else if (e.keyCode == '38') {
            // up arrow
        }
        else if (e.keyCode == '40') {
            // down arrow
        }
        else if (e.keyCode == '37') {
           // left arrow
            position--;
            if(position<0) { position = descrizioni.length-1; }
            showDescription(descrizioni, position, total_number);
        }
        else if (e.keyCode == '39') {
           // right arrow
            position++;
            if(position>descrizioni.length-1) { position = 0; }
            showDescription(descrizioni, position, total_number);
        }
    }
}

/* Gesitione hover */ 
    $('#listaVoci ul').on("mouseleave touchmove", function(){
        // console.log("enter");
        $('.voce-image img').css('filter','saturate(100%)');
    })
    $('#listaVoci li').on("touchstart mouseenter", function(){

        $('.voce-image img').css('filter','saturate(25%)');
        $('img',this).css('filter','saturate(100%)');
    }).on("mouseleave touchmove", function(){

    }).on('click', function(){
        showDescription(descrizioni, descrizioni.indexOf($(this).attr("data-id")) , total_number);
        $('.full').css('opacity',1).css('visibility','visible');
        setTimeout(function(){ 
            $('header .memoria').addClass('blurred');
            $('.copertina').addClass('blurred');
            $('.voce').addClass('blurred');
            $('footer').addClass('blurred');
            $('.credit').addClass('blurred');             
        }, 300);
    });

     $('span.close').on('click', function(){
        $('.full').css('opacity',0).css('visibility','hidden');
       $('.voce').removeClass('blurred');
       $('header .memoria').removeClass('blurred');
       $('footer').removeClass('blurred');
       $('.credit').removeClass('blurred');
       $('.copertina').removeClass('blurred');
    });   

    document.addEventListener('click', function (event) {
        if( $(event.target).hasClass('full')) {
            $('.full').css('opacity',0).css('visibility','hidden');
            $('span.close').trigger('click');
        }
    });

    $('a.linke').on('click', function(){
        // console.log("click");
        //$(this).removeClass('tooltip');
        returnToList = $(this).offset().top;
        $("html, body").animate({ scrollTop: $('#long_desc').offset().top }, 1000);
        showDescription(descrizioni, descrizioni.indexOf($(this).attr("data-id")) , total_number);
    });

    $('a.credits').on('click', function(){
        $("html, body").animate({ scrollTop: $('#crediti').offset().top }, 1000);
    });

    // browser window scroll (in pixels) after which the "back to top" link is shown
        offset = 300,
    //browser window scroll (in pixels) after which the "back to top" link opacity is reduced
        offset_opacity = 1200,
    //duration of the top scrolling animation (in ms)
        scroll_top_duration = 700,
    //grab the "back to top" link
        $back_to_top = $('.cd-top');
        $scrollup = $('.scrollup');

    //hide or show the "back to top" link
    $(window).scroll(function(){
        ( $(this).scrollTop() > offset ) ? $back_to_top.addClass('cd-is-visible') : $back_to_top.removeClass('cd-is-visible cd-fade-out');
        if( $(this).scrollTop() > offset_opacity ) { 
            $back_to_top.addClass('cd-fade-out');
        }
    });

    pop = 0
    $('.pop').on('click', function(event){
        console.log(pop);
        if (pop == "0") {
            $('.shareToolBar a').css('display','block');
            pop++
        }
        else  {
            $('.shareToolBar a').css('display','none');
            $(this).css('display','block');            
            pop--
        }
        console.log(pop);
    });


    //smooth scroll to top
    $back_to_top.on('click', function(event){
        event.preventDefault();
        $('span.close').trigger('click');
        $('body,html').animate({
            scrollTop: 0 ,
            }, scroll_top_duration
        );
    });
    $scrollup.on('click', function(event){
        event.preventDefault();
        $('span.close').trigger('click');
        $('body,html').animate({
            scrollTop: 0 ,
            }, scroll_top_duration
        );
    });

/* animazione iniziale */

    // $('.loader img').animate({
    //     opacity: 1,
    //     easing: 'linear'
    // }, 1000, function(){

    // });

    $('.loader .fiore').animate({
        width: '160px',
        opacity: 1,
        easing: 'easeOutCirc'
    }, 6000, function(){

    });
    $('.loader .rotate').delay(2000).animate({
        width: '240px',
        easing: 'linear'
    }, 4000, function(){

    });

	var winH = $(window).height();
	console.log(winH);


});

$(window).load( function(){ // load all object

    var numItems = $('.voce').not('.regione').length;

    //$('.loader span').addClass('loaded');
    $('.loader span').prop('Counter',0).animate({
        //Counter: $('.loader span').text()
        Counter: numItems
    }, {
        duration: 2000,
        easing: 'easeInOutQuint',
        step: function (now) {
            $('.loader span').text(Math.ceil(now));
        },
        complete: function () {
            $('.loader').delay(500).animate({
                easing: 'linear',
                "opacity":0
            }, 1500, function(){
                $(this).hide("fast");
                console.log("numero voci: "+numItems);
            });
        }
    });

    $(".voce-image").each(function() {  
        wdt = $(this).css('width');
        $(this).css('height',wdt);
    });
});

$( window ).resize(function() {  // window browser resize
    $(".voce-image").each(function() {  
        wdt = $(this).css('width');
        $(this).css('height',wdt);
    });
});