<title>Morti in Italia per Coronavirus: lo Speciale Addii - Corriere.it</title>
<meta name="title" content="Morti in Italia per Coronavirus: lo Speciale Addii">
<meta name="description" content="I morti in Italia per Coronavirus: giovani, anziani, medici, infermieri e non solo. A loro, Corriere dedica lo Speciale Addii, con commenti di grandi firme.">
<meta name="keywords" content="">
<meta name="viewport" content="width=device-width,initial-scale=1">
<meta property="fb:app_id" content="203568503078644">
<meta property="og:title" content="Morti in Italia per Coronavirus: lo Speciale Addii">
<meta property="og:description" content="I morti in Italia per Coronavirus: giovani, anziani, medici, infermieri e non solo. A loro, Corriere dedica lo Speciale Addii, con commenti di grandi firme.">
<meta property="og:type" content="article">
<meta property="og:locale" content="it_IT">
<meta property="og:url" itemprop="url" content="https://www.corriere.it/speciale/cronache/2020/addii-morti-coronavirus-italia/">
<meta property="og:image" content="">
<meta property="og:site_name" content="Corriere della Sera">
<meta name="author" content="Corriere.it">
<meta name="twitter:card" content="summary_large_image">
<meta name="twitter:site" content="@Corriere">
<meta name="twitter:creator" content="@Corriere">
<meta name="twitter:title" content="Morti in Italia per Coronavirus: lo Speciale Addii">
<meta name="twitter:description" content="I morti in Italia per Coronavirus: giovani, anziani, medici, infermieri e non solo. A loro, Corriere dedica lo Speciale Addii, con commenti di grandi firme.">
<meta name="twitter:image" content="">
<link rel="canonical" href="https://www.corriere.it/speciale/cronache/2020/addii-morti-coronavirus-italia/">
<meta property="vr:canonical" content="https://www.corriere.it/speciale/cronache/2020/addii-morti-coronavirus-italia/">
