/*

  Libreria degli helper handlebar
  Utilizzati nella creazione dei template

*/


/*
  Aggionge la possibilità del log all'interno dei template in fase di compilazione.
*/
handlebars.registerHelper("log", function(something) {
  console.log(something);
});




// HELPER DI CONFRONTO

/*
  Verificas l'uguaglianza semplice == tra i due elementi
*/
handlebars.registerHelper("equals", function (a, b) {
  return (a == b);
});


/*
  Gestisce le condizioni all'interno dei template handlebar
*/
handlebars.registerHelper('ifCond', function (v1, operator, v2, options) {

    switch (operator) {
        case '==':
            return (v1 == v2) ? options.fn(this) : options.inverse(this);
        case '===':
            return (v1 === v2) ? options.fn(this) : options.inverse(this);
        case '<':
            return (v1 < v2) ? options.fn(this) : options.inverse(this);
        case '<=':
            return (v1 <= v2) ? options.fn(this) : options.inverse(this);
        case '>':
            return (v1 > v2) ? options.fn(this) : options.inverse(this);
        case '>=':
            return (v1 >= v2) ? options.fn(this) : options.inverse(this);
        case '&&':
            return (v1 && v2) ? options.fn(this) : options.inverse(this);
        case '||':
            return (v1 || v2) ? options.fn(this) : options.inverse(this);
        case '!=':
            return (v1 != v2) ? options.fn(this) : options.inverse(this);
        default:
            return options.inverse(this);
    }
});



/*
  Converte le stringe unicode nella codifica di html entities compatibile coi nostri server in iso8959-1
*/
handlebars.registerHelper("normalizzami", function(input) {
  
  if ( input == "" || input == undefined || input == null ){
    output = "";
    return new handlebars.SafeString(output);
    
  } else {
    // output = html_encode(input);
    input = input.replace(/[^\w ]/g, function(char) {
      return dictionary[char] || char;
    });
    
    return new handlebars.SafeString(input);
    
  }
});

/*
  Toglie gli spazi a inizio e fine
*/
handlebars.registerHelper("trimmami", function(input) {
  if ( input == "" || input == undefined || input == null ){
    output = "";
    return output;
  } else {
    // output = html_encode(input);
    input = input.replace(/^ +/, '').replace(/ +$/, '').replace(/[^\w ]/g, function(char) {
      return dictionary_slug[char] || char;
    });
    return input;
  }
});

/*
  Toglie gli spazi all'interno della stringa in ingresso
*/
handlebars.registerHelper("noSpace", function(input) {
  if ( input == "" || input == undefined || input == null ){
    output = "";
    return output;
  } else {
    // output = html_encode(input);
    input = input.replace(/\./g, "").replace(/\s+/g, '-').replace(/\-\-+/g, '-').replace(/^-+/, '').replace(/-+$/, '').replace(/[^\w ]/g, function(char) {
      return dictionary_slug[char] || char;
    });
    return input;
  }
});


    // .replace(/\s+/g, '-')           // Replace spaces with -
    // .replace(/\-\-+/g, '-')         // Replace multiple - with single -
    // .replace(/^-+/, '')             // Trim - from start of text
    // .replace(/-+$/, '');            // Trim - from end of text


/*
 * handlebars Helper: Moment.js
 * @author: https://github.com/Arkkimaagi
 * Built for Assemble: the static site generator and
 * component builder for Node.js, Grunt.js and Yeoman.
 * http://assemble.io
 *
 * Copyright (c) 2014, Brian Woodward, Jon Schlinkert
 * Licensed under the MIT license.
 */

/*jshint node:true */

  handlebars.registerHelper('moment', function (context, block) {
    if (context && context.hash) {
      block = _.cloneDeep(context);
      context = undefined;
    }
    var date = moment(context);
    var hasFormat = false;

    // Reset the language back to default before doing anything else
    date.locale('it_IT');
    // date = iconv.encode(date, "ISO-8859-1");

    for (var i in block.hash) {
      if (i === 'format') {
        hasFormat = true;
      }
      else if (date[i]) {
        date = date[i](block.hash[i]);
      } else {
        console.log('moment.js does not support "' + i + '"');
      }
    }

    if (hasFormat) {
      date = date.format(block.hash.format);
    }
    date = date.replace(/[^\w ]/g, function(char) {
      return dictionary[char] || char;
    });
    // date = iconv.encode(date, "UTF-8")
    return date;
  });

  handlebars.registerHelper('duration', function (context, block) {
    if (context && context.hash) {
      block = _.cloneDeep(context);
      context = 0;
    }
    var duration = moment.duration(context);
    var hasFormat = false;

    // Reset the language back to default before doing anything else
    duration = duration.locale('it_IT');

    for (var i in block.hash) {
      if (i === 'format') {
        hasFormat = true;
      }
      else if (duration[i]) {
        duration = duration[i](block.hash[i]);
      } else {
        console.log('moment.js duration does not support "' + i + '"');
      }
    }

    if (hasFormat) {
      duration = duration.format(block.hash.format);
    }
    duration = duration.replace(/[^\w ]/g, function(char) {
      return dictionary[char] || char;
    });
    return duration;
  });


handlebars.registerHelper("getValueObjKey", function (obj, key) {
  // console.log(obj[key]);
  return obj[key];
});


/**
  * Nei template multipagina serve per avere il link alle pagine interne
  */
handlebars.registerHelper("multiPageLink", function (obj, paths, filename) {

  _link = "";
  
  if(paths == "../page"){
    
    _link += "page="
  
  } else {
  
    paths.forEach(function(v,k){
      _link += obj[v] + "/";
    });
  
  }

  _link += slugify(obj[filename]);
   console.log(_link); 
  return new handlebars.SafeString(_link);
  
});



var dictionary = require("./dictionary.js");
var dictionary_slug = require("./dictionary_slug.js");

function slugify(text)
{
  return text.toString().toLowerCase()
    .replace(/\s+/g, '-')           // Replace spaces with -
    .replace(/[^\w\-]+/g, '')       // Remove all non-word chars
    .replace(/\-\-+/g, '-')         // Replace multiple - with single -
    .replace(/^-+/, '')             // Trim - from start of text
    .replace(/-+$/, '');            // Trim - from end of text
}